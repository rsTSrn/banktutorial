
public class SavingAccount extends BankAccount {

	private final double  INTEREST_RATE = 0.05;
	
	public SavingAccount(){
		
		super();
	}
	
	public SavingAccount(String owner , double initialBalance){
		
		super(owner,initialBalance);	
	}
	public void AddInterest(){
		
		double increaseBy = (this.getInitialBalance() * INTEREST_RATE);
		this.Deposit(increaseBy);
	}
	
	
}
